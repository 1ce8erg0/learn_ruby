def translate(phrase)
  phrase.split.map do |word|
    v = first_vowel(word)
    word[v..-1] + word[0,v] + "ay"
  end.join(" ")
end

def first_vowel(word)
  if word =~ /^qu/
    2
  elsif word =~ /^squ/
    3
  else
    word.gsub(/[aeiou].*$/, '').size
  end
end
