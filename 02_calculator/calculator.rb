def add(a,b)
	return a + b
end

def subtract(a,b)
	return a - b
end

def sum(a)
	return a.inject(0){|result,sum| result += sum}
# hash = [[:first_name, 'Shane'], [:last_name, 'Harvie']].inject({}) do |result, element|
#  result[element.first] = element.last
#  result
# http://blog.jayfields.com/2008/03/ruby-inject.html

end

def multiply(a)
	return a.inject(0){|results,mult| results *= mult}
end

def power(a,b)
	return a ** b
end

def factorial(a,b)
	return a/b
end

