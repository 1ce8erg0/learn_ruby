def echo(says)
	"#{says}"
end

def shout(says)
	"#{says}".upcase
	# puts 'one TWO three foUR'.split.map(&:capitalize).join(' ')
end

def repeat(says, i = 2)
	("#{says} " * i).strip
end

def start_of_word(s, i)
	return s[0..i-1]
end

def first_word(s)
	return s.split[0]
end

def titleize(s)

	# s = s.split.map(&:capitalize).join(' ')
	# except_words = ["for", "over", "of", "in", "And"]

	# words = s.split(" ")
	# words.each do |word|
	# 	puts word
	# 	if((except_words).include?(word))
	# 		puts "found"
	# 		word = word.downcase
	# 		puts word
	# 	else puts "not found"


	# 	end
	# end

	# words.join
	# puts words

	# return s.strip
	# words = s.split(" ")
	# except_words = ["for", "over", "of", "in", "and"]
	# result = ""

	# words.each do |word|
	# 	if ((except_words).include?(word))
	# 		word.downcase
	# 	else
	# 		word.capitalize
	# 	end
	# end

	# words[0].capitalize

	# words.each do |word|
	# 	result += word + " "
	# end
	# puts result.strip
	# return result.strip

   words = s.split.map do |word|
    if %w(the and over).include?(word)
      word
    else
      word.capitalize
    end
  end
  # words.first.capitalize!
  words[0] = words[0].capitalize
  words.join(" ")
end
